<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
      'category_parent_id',
      'name',
    ];

    /**
     * @return BelongsTo
     */
    public function categoryParent():BelongsTo
    {
        return $this->belongsTo(CategoryParent::class);
    }

    /**
     * @return BelongsToMany
     */
    public function products():BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}
